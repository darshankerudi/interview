CREATE TABLE posts (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    content TEXT NOT NULL
);
<!DOCTYPE html>
<html>
<head>
    <title>PHP/MySQL CRUD Example</title>
</head>
<body>
    <h1>Create a Post</h1>
    <form method="post" action="create_post.php">
        <label for="title">Title:</label>
        <input type="text" name="title" id="title">
        <br><br>
        <label for="content">Content:</label>
        <textarea name="content" id="content"></textarea>
        <br><br>
        <input type="submit" value="Create">
    </form>
</body>
</html>
<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "person";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Prepare and bind parameters
$stmt = $conn->prepare("INSERT INTO posts (title, content) VALUES (?, ?)");
$stmt->bind_param("ss", $title, $content);

// Set parameters and execute
$title = $_POST["title"];
$content = $_POST["content"];
$stmt->execute();

echo "Post created successfully";

$stmt->close();
$conn->close();
?>
<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "person";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Retrieve posts from database
$sql = "SELECT * FROM posts";
$result = $conn->query($sql);

// Display posts
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        echo "<h2>" . $row["title"] . "</h2>";
        echo "<p>" . $row["content"] . "</p>";
        echo "<a href='edit_post.php?id=" . $row["id"] . "'>Edit</a> ";
        echo "<a href='delete_post.php
