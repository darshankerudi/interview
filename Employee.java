

	
	
		
		import java.io.IOException;
		import org.jsoup.Jsoup;
		import org.jsoup.nodes.Document;
		import org.jsoup.nodes.Element;
		import org.jsoup.select.Elements;

		public class Employee {

		    public static void main(String[] args) {
		        String url = "https://stackoverflow.com/questions/tagged/python";

		        try {
		            Document doc = Jsoup .connect(url).get();
		            Elements questions = doc.select(".question-summary .question-hyperlink");

		            for (Element question : questions) {
		                System.out.println(question.text());
		            }

		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}

		
		
